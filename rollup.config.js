import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import typescript from 'rollup-plugin-typescript2';
import svgr from '@svgr/rollup';
import postcss from 'rollup-plugin-postcss';
import babel from '@rollup/plugin-babel';

import packageJson from './package.json';

export default {
  external: ['react', 'socket.io-client', 'react-dom'],
  input: './src/index.ts',
  output: [
    {
      file: packageJson.main,
      format: 'es',
      name: '@morphysm/react-chat-widget',
      sourcemap: true,
    },
  ],
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript(),
    svgr({
      svgoConfig: {
        plugins: {
          removeViewBox: false,
        },
      },
    }),
    postcss(),
    babel({
      presets: ['@babel/react'],
    }),
  ],
};
