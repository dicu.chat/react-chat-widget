<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assests/morph_logo_rgb.png" alt="Morphysm" ></a>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">
 <img src="https://img.shields.io/npm/v/npm" alt="npm badge">
 <img src="https://img.shields.io/badge/React-17.0.2-orange" alt="react badge">
 <img src="https://img.shields.io/badge/version-1.1.8-green" alt="version badge">
</h1>
 
# Getting Started
This react component allows you to add the Dicu.chat AI chatbot to your react app.

# How It Works

The widget communicates with the web bridge of the core server using a web socket "[socket.io-client](https://www.npmjs.com/package/socket.io-client)", it internally stores all messages received through the web socket and displays them in the widget UI. To respond the user can select buttons or write text that is returned through the websocket.

You can add the widget to your react app as a react component. Using parameters, you can add the widget in fullscreen or as a in window pop up.
Furthermore, you can customise icons and toggle the audio feature.



# Installing
To run this project you have to following these steps:

1.  Git clone this repo:

    ```
     git clone git@gitlab.com:dicu.chat/react-chat-widget.git
    ```
2.  Run npm install:

    ```
    npm i 
    ```

# Develop

In order to run our widget locally you can use this command:

1.  Npm run start:

    ```
    npm run storybook
    ```


<u>***Note***</u>:
To use this widget to to replace our test conversational flow with a localy hosted conversational flow, change the WEBSOCKET URL inside the file names widget.stories.tsx and fullscreen.stories.tsx to a locally hosted URL.
`const WEBSOCKET_URL = 'ws://localhost:8082';`
Also, keep in mind that the server and bridge websocket repositories must be run concurrently with this repo. Otherwise, the widget will follow the conversational flow of our default bot.


# How to Integrate
To add our widget to your **react app** please fellow these steps:

1. Install our library:
   ```
   npm i @morphysm/react-chat-widget
   ```
2. Add the widget to your react app:

```js
import * as React from 'react';
import { Widget } from '@morphysm/react-chat-widget';

export interface AppProps {
  websocketURL: string;
  audioEnabled: boolean;
  userIcon?: string;
  closeIcon?: string;
  sendIcon?: string;
  widgetIcon?: string;
}

const App: React.FC<AppProps> = ({
  websocketURL,
  audioEnabled,
  userIcon,
  closeIcon,
  sendIcon,
  widgetIcon,
}: AppProps) => {
  return (
    <div className='App'>
      <Widget
        websocketURL={websocketURL}
        audioEnabled={audioEnabled}
        userIcon={userIcon}
        closeIcon={closeIcon}
        sendIcon={sendIcon}
        widgetIcon={widgetIcon}
      />
    </div>
  );
};

export default App;

```



| Name                | Type               |  Description  
| --------------------| ------------------ | ----------------------|
| websocketURL        | string             | URL -Used to establish a websocket connection to the dicu.chat web bridge.             |
| audioEnabled        | boolean             | Toggles voice messages      |
| userIcon            | string             | URL -Path to custom user icon    |
| closeIcon           | string             | URL -Path to custom close icon   |
| sendIcon            | string             | URL -Path to custom send icon    |
| widgetIcon          | string             | URL -Path to custom widget icon  |



# Code Owners

@morphysm/team    :sunglasses:

# License
Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/react-chat-widget/-/blob/master/LICENSE)


# Contact
If you'd like to know more about  us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".






