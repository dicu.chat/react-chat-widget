export { default as Widget } from './components/organisms/widget/Widget';
export { default as FullScreen } from './components/organisms/fullScreen/FullScreen';
