import React from 'react';
import Widget from './components/organisms/widget/Widget';
import MicIcon from './static/mic.svg';
import CloseIcon from './static/close.svg';
import SendIcon from './static/send.svg';
import UserIcon from './static/account.svg';

const WEBSOCKET_URL = 'wss://09ee4ec2aa.a91ad78fad90f2d5aa4f51e0.com';
const AUDIO_ENABLED = true;

export default {
  title: 'Widget',
  component: Widget,
};

export const Default = (): React.ReactNode => (
  <Widget websocketURL={WEBSOCKET_URL} audioEnabled={AUDIO_ENABLED} />
);

export const SVGSwitch = (): React.ReactNode => (
  <Widget
    websocketURL={WEBSOCKET_URL}
    audioEnabled={AUDIO_ENABLED}
    userIcon={CloseIcon}
    closeIcon={MicIcon}
    micIcon={CloseIcon}
    sendIcon={UserIcon}
    widgetIcon={SendIcon}
  />
);

export const IMGLink = (): React.ReactNode => (
  <Widget
    websocketURL={WEBSOCKET_URL}
    audioEnabled={AUDIO_ENABLED}
    userIcon={'https://picsum.photos/40'}
    closeIcon={'https://picsum.photos/40'}
    sendIcon={'https://picsum.photos/40'}
    widgetIcon={'https://picsum.photos/40'}
    micIcon={'https://picsum.photos/40'}
  />
);
