import React from 'react';
import { useState, useEffect } from 'react';
import { base64ToURL } from '../../../services/audioTranspiler';
import { ContentTypes, File } from '../../../services/types';
import './MessageAudio.css';

interface MessageAudioProps {
  file: File;
}

const MessageAudio: React.FC<MessageAudioProps> = ({
  file,
}: MessageAudioProps) => {
  const [url, setUrl] = useState('');

  useEffect(() => {
    if (file.contentType === ContentTypes.Bytes) {
      base64ToURL(file.content, file.extension, file.codec).then((url) => {
        setUrl(url);
      });
      return;
    }
    setUrl(file.content);
  }, [file]);

  return <audio className='MessageAudio' src={url} controls />;
};

export default MessageAudio;
