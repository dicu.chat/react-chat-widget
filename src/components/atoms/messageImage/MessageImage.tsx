import React from 'react';
import './MessageImage.css';

interface MessageImageProps {
  url: string;
}

const MessageImage: React.FC<MessageImageProps> = ({
  url,
}: MessageImageProps) => {
  return <img className='MessageImage' src={url} alt='img' />;
};

export default MessageImage;
