import React from 'react';
import './IconButton.css';

interface IconButtonProps {
  Icon?: Icon;
  onClick: () => any;
}

const IconButton: React.FC<IconButtonProps> = ({
  Icon,
  onClick,
}: IconButtonProps) => {
  let icon;
  switch (typeof Icon) {
    // Allows for buttons without icons
    case 'undefined': {
      break;
    }
    case 'string': {
      icon = <img src={Icon} alt='userIcon' className='IconButtonIcon' />;
      break;
    }
    default: {
      icon = <Icon className='IconButtonIcon' />;
      break;
    }
  }

  return (
    <button className='IconButton' onClick={onClick}>
      {icon}
    </button>
  );
};

export default IconButton;
