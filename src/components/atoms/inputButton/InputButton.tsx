import React from 'react';
import SendIcon from '../../../static/send.svg';
import MicIcon from '../../../static/mic.svg';
import './InputButton.css';
import IconButton from '../iconButton/IconButton';

interface InputButtonProps {
  showSend: boolean;
  onClick: () => void;
  sendIcon?: Icon;
  micIcon?: Icon;
}

const InputButton: React.FC<InputButtonProps> = ({
  showSend,
  onClick,
  sendIcon,
  micIcon,
}: InputButtonProps) => {
  const handleClick = () => {
    onClick();
  };

  const button = () => {
    if (showSend) {
      return (
        <div className='InputButton'>
          <IconButton
            Icon={sendIcon ? sendIcon : SendIcon}
            onClick={handleClick}
          />
        </div>
      );
    } else {
      return (
        <div className='InputButton InputButtonAudio'>
          <IconButton
            Icon={micIcon ? micIcon : MicIcon}
            onClick={handleClick}
          />
        </div>
      );
    }
  };

  return button();
};

export default InputButton;
