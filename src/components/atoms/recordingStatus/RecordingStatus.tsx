import React from 'react';
import './RecordingStatus.css';

interface RecordingStatusProps {
  recordingTime: number;
}

const RecordingStatus: React.FC<RecordingStatusProps> = ({
  recordingTime,
}: RecordingStatusProps) => {
  const sToMmSs = (absSeconds: number) => {
    const seconds = String(absSeconds % 60);
    const minutes = String(Math.floor(absSeconds / 60));

    return `${minutes.padStart(2, '0')}:${seconds.padStart(2, '0')}`;
  };

  return (
    <div className='RecordingStatus'>
      <span className='RecordingStatusTime'>{sToMmSs(recordingTime)}</span>
      <div className='RecordingStatusDot' />
    </div>
  );
};

export default RecordingStatus;
