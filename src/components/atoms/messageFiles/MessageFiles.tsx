import React from 'react';
import { File, FileTypes } from '../../../services/types';
import MessageImage from '../messageImage/MessageImage';
import MessageAudio from '../messageAudio/MessageAudio';
import MessageVideo from '../messageVideo/MessageVideo';
import './MessageFiles.css';

interface MessageFilesProps {
  files: File[];
}

const MessageFiles: React.FC<MessageFilesProps> = ({
  files,
}: MessageFilesProps) => {
  const fileComponents = () => {
    return files.map((file) => {
      console.log(file);
      if (file.mediaType === FileTypes.Image) {
        return <MessageImage url={file.content} />;
      }
      if (file.mediaType === FileTypes.Voice) {
        return <MessageAudio file={file} />;
      }
      if (file.mediaType === FileTypes.Video) {
        return <MessageVideo url={file.content} />;
      }
      return undefined;
    });
  };

  return <div className='MessageFiles'>{fileComponents()}</div>;
};

export default MessageFiles;
