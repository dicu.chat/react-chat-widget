import React from 'react';
import './UserImage.css';

interface UserImageProps {
  Icon: Icon;
}

const UserImage: React.FC<UserImageProps> = ({ Icon }: UserImageProps) => {
  if (typeof Icon === 'string') {
    return <img src={Icon} alt='userIcon' className='UserImage' />;
  }

  return <Icon className='UserImage' />;
};

export default UserImage;
