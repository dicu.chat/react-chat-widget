import React from 'react';
import './MessageVideo.css';

interface MessageVideoProps {
  url: string;
}

const MessageVideo: React.FC<MessageVideoProps> = ({
  url,
}: MessageVideoProps) => {
  return (
    <video className='MessageVideo' controls autoPlay muted>
      <source src={url} type='video/mp4' />
      Your browser does not support the videos.
    </video>
  );
};

export default MessageVideo;
