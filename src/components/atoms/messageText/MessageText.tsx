import React from 'react';
import reactStringReplace from 'react-string-replace';
import './MessageText.css';

interface MessageTextProps {
  text: string;
  commands?: string[];
  onCommandClick: (string) => void;
}

const MessageText: React.FC<MessageTextProps> = ({
  text,
  commands,
  onCommandClick,
}: MessageTextProps) => {
  const addLinks = (text: string) => {
    if (!text) return;

    const commandRegex = '(' + commands.join('|') + ')';
    let content = reactStringReplace(
      text,
      new RegExp(commandRegex),
      (match) => {
        return (
          <span
            className='MessageTextCommand'
            onClick={() => handleCommandClick(match)}
          >
            {match}
          </span>
        );
      }
    );
    content = reactStringReplace(content, /(https?:\/\/\S+)/g, (url) => {
      return (
        <a href={url} target='_blank' rel='noopener noreferrer'>
          {url}
        </a>
      );
    });

    return content;
  };

  const handleCommandClick = (command: string) => {
    onCommandClick(command);
  };

  return <div className='MessageText'> {addLinks(text)} </div>;
};

export default MessageText;
