import React from 'react';
import './MessageButton.css';

interface MessageButtonProps {
  text: string;
  onClick: (text: string) => void;
}

const MessageButton: React.FC<MessageButtonProps> = ({
  text,
  onClick,
}: MessageButtonProps) => {
  const handleClick = () => {
    onClick(text);
  };

  return (
    <button className='MessageButton' onClick={handleClick}>
      {text}
    </button>
  );
};

export default MessageButton;
