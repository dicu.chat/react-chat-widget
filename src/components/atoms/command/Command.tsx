import React from 'react';
import { sendTextMessage } from '../../../services/socket';
import './Command.css';

interface CommandProps {
  name: string;
  description: string;
  closeCallback: () => void;
}

const Command: React.FC<CommandProps> = ({
  name,
  description,
  closeCallback,
}: CommandProps) => {
  const handleClick = () => {
    sendTextMessage(name);
    closeCallback();
  };

  return (
    <div className='Command' onClick={handleClick}>
      <b>{name}</b>
      <i>{description}</i>
    </div>
  );
};

export default Command;
