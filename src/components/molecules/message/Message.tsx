import React from 'react';
import { InternalMessage, File } from '../../../services/types';
import { getCommands } from '../../../services/socketState';
import { sendTextMessage } from '../../../services/socket';
import MessageFiles from '../../atoms/messageFiles/MessageFiles';
import MessageText from '../../atoms/messageText/MessageText';
import './Message.css';

interface MessageProps {
  messageBody: InternalMessage;
}

const Message: React.FC<MessageProps> = ({ messageBody }: MessageProps) => {
  const handleCommandClick = (text: string) => {
    sendTextMessage(text);
  };

  const files = () => {
    console.log('messageBody', messageBody);
    //TODO workaround
    let messageFiles: File[] = [];
    if (messageBody.files) {
      messageFiles = [...messageBody.files];
    }
    if (messageBody.message.voice) {
      messageFiles.push(messageBody.message.voice);
    }

    if (messageFiles) {
      return <MessageFiles files={messageFiles} />;
    }
  };

  const text = () => {
    if (messageBody.message.text) {
      return (
        <MessageText
          text={messageBody.message.text}
          commands={getCommands().map((command) => command.name)}
          onCommandClick={handleCommandClick}
        />
      );
    }
  };

  const messageContent = () => {
    return (
      <div className='Message'>
        {files()}
        {text()}
      </div>
    );
  };

  return messageContent();
};

export default Message;
