import React from 'react';
import { InternalMessage } from '../../../services/types';
import Message from '../message/Message';

import './MessageRow.css';

interface MessageRowProps {
  messageBody: InternalMessage;
}

const MessageRow: React.FC<MessageRowProps> = ({
  messageBody,
}: MessageRowProps) => {
  return (
    <div
      className={`MessageRow${
        messageBody.isBot ? ' MessageBot' : ' MessageUser'
      }`}
    >
      <Message messageBody={messageBody} />
    </div>
  );
};

export default MessageRow;
