import React from 'react';
import { Button } from '../../../services/types';
import { sendTextMessage } from '../../../services/socket';
import MessageButton from '../../atoms/messageButton/MessageButton';
import './ButtonRow.css';

interface ButtonRowProps {
  buttons: Button[];
}

const ButtonRow: React.FC<ButtonRowProps> = ({ buttons }: ButtonRowProps) => {
  const handleButtonClick = (text: string) => {
    sendTextMessage(text);
  };

  return (
    <div className='ButtonRow'>
      {buttons.map((button, idx) => (
        <MessageButton
          key={idx}
          text={button.text}
          onClick={handleButtonClick}
        />
      ))}
    </div>
  );
};

export default ButtonRow;
