import React from 'react';
import { useEffect, useState } from 'react';
import { Command as CommandI } from '../../../services/types';
import { getCommands } from '../../../services/socketState';
import Command from '../../atoms/command/Command';
import './Commands.css';

interface CommandsProps {
  closeCallback: () => void;
}

const Commands: React.FC<CommandsProps> = ({
  closeCallback,
}: CommandsProps) => {
  const [commands, setCommands] = useState<CommandI[]>([]);

  useEffect(() => {
    setCommands(getCommands);
  }, []);

  const handleCommandClose = () => {
    closeCallback();
  };

  return (
    <div className='Commands'>
      {commands.map((command) => (
        <Command
          name={command.name}
          description={command.description}
          closeCallback={handleCommandClose}
        />
      ))}
    </div>
  );
};

export default Commands;
