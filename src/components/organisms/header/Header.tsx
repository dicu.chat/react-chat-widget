import React from 'react';
import UserIcon from '../../../static/account.svg';
import CloseIcon from '../../../static/close.svg';
import IconButton from '../../atoms/iconButton/IconButton';
import UserImage from '../../atoms/userImage/UserImage';
import './Header.css';

interface HeaderProps {
  closeCallback?: () => void;
  userIcon?: Icon;
  closeIcon?: Icon;
}

const Header: React.FC<HeaderProps> = ({
  closeCallback,
  userIcon,
  closeIcon,
}: HeaderProps) => {
  return (
    <div className='Header'>
      <div className='HeaderContent'>
        <UserImage Icon={userIcon ? userIcon : UserIcon} />
        {closeCallback && (
          <IconButton
            Icon={closeIcon ? closeIcon : CloseIcon}
            onClick={closeCallback}
          />
        )}
      </div>
    </div>
  );
};

export default Header;
