import React from 'react';
import { useState } from 'react';
import { useRecorder } from '../../../hooks/useRecorder';
import { sendTextMessage, sendAudioMessage } from '../../../services/socket';
import InputButton from '../../atoms/inputButton/InputButton';
import RecordingStatus from '../../atoms/recordingStatus/RecordingStatus';
import Commands from '../../molecules/commands/Commands';
import './Input.css';

interface InputProps {
  micIcon?: Icon;
  sendIcon?: Icon;
}

const Input: React.FC<InputProps> = ({ micIcon, sendIcon }: InputProps) => {
  const [text, setText] = useState('');
  const [
    isRecoding,
    recordingTime,
    startRecoding,
    stopRecoding,
  ] = useRecorder();

  const handleTextChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(event.target.value);
  };

  const handleSend = async () => {
    if (isRecoding) {
      const blob = await stopRecoding();
      if (!blob) {
        //TODO handle error
        console.error('no recoding data');
      } else {
        sendAudioMessage(blob, text ? text : undefined);
        setText('');
      }
    } else if (text) {
      sendTextMessage(text);
      setText('');
    }
  };

  const onEnterPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleSend();
    }
  };

  const handleButtonClick = () => {
    if (text || isRecoding) {
      handleSend();
    } else {
      startRecoding();
    }
  };

  const handleCommandClose = () => {
    setText('');
  };

  const commands = () => {
    if (text.startsWith('/')) {
      return <Commands closeCallback={handleCommandClose} />;
    }
  };

  const recording = () => {
    if (isRecoding) {
      return <RecordingStatus recordingTime={recordingTime} />;
    }
  };

  return (
    <div className='Input'>
      {commands()}
      <textarea
        className='InputTextarea'
        value={text}
        onChange={handleTextChange}
        onKeyDown={onEnterPress}
      />
      {recording()}
      <InputButton
        showSend={isRecoding || text !== ''}
        onClick={handleButtonClick}
        sendIcon={sendIcon}
        micIcon={micIcon}
      />
    </div>
  );
};

export default Input;
