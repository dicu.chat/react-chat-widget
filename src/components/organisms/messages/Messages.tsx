import React from 'react';
import { useState, useEffect, useRef } from 'react';
import useResizeObserver from '@react-hook/resize-observer';
import { startSocket } from '../../../services/socket';
import { getMessages } from '../../../services/socketState';
import { InternalMessage } from '../../../services/types';
import ButtonRow from '../../molecules/buttonRow/ButtonRow';
import MessageRow from '../../molecules/messageRow/MessageRow';
import './Messages.css';

const MESSAGE_KEY = 'message_';

interface MessagesProps {
  websocketURL: string;
}

const Messages: React.FC<MessagesProps> = ({ websocketURL }: MessagesProps) => {
  const messagesColumnRef = useRef<HTMLDivElement>(null);
  const messagesEndRef = useRef<HTMLDivElement>(null);
  let [messages, setMessages] = useState<InternalMessage[]>([]);

  const queryMessages = () => {
    setMessages(getMessages());
  };

  // TODO find better solution
  useResizeObserver(messagesColumnRef, () => {
    scrollToBottom();
  });

  // Add callback to ws and query initial set of messages
  useEffect(() => {
    const onMessage = () => {
      queryMessages();
    };

    startSocket(websocketURL, onMessage);
    queryMessages();
  }, [websocketURL]);

  // Scroll to bottom
  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  const scrollToBottom = () => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
      });
    }
  };

  const messageRows = () => {
    return messages.map((message, idx) => (
      <MessageRow key={`${MESSAGE_KEY}${idx}`} messageBody={message} />
    ));
  };

  const buttonRow = () => {
    if (messages.length === 0) {
      return;
    }
    const lastMessage = messages[messages.length - 1];
    if (lastMessage.buttons.length) {
      return <ButtonRow buttons={lastMessage.buttons} />;
    }
  };

  return (
    <div className='Messages'>
      <div className='MessagesColumn' ref={messagesColumnRef}>
        {messageRows()}
        {buttonRow()}
      </div>
      <div ref={messagesEndRef} />
    </div>
  );
};

export default Messages;
