import React from 'react';
import Header from '../header/Header';
import Input from '../input/Input';
import Messages from '../messages/Messages';
import NoAudioInput from '../noAudioInput/NoAudioInput';
import './FullScreen.css';

export interface FullScreenProps {
  websocketURL: string;
  audioEnabled?: boolean;
  userIcon?: Icon;
  micIcon?: Icon;
  sendIcon?: Icon;
}

const FullScreen: React.FC<FullScreenProps> = ({
  websocketURL,
  audioEnabled,
  userIcon,
  micIcon,
  sendIcon,
}: FullScreenProps) => {
  return (
    <div className='FullScreen'>
      <div className='FullScreenHeader'>
        <Header userIcon={userIcon} />
      </div>

      <Messages websocketURL={websocketURL} />
      {audioEnabled ? (
        <Input sendIcon={sendIcon} micIcon={micIcon} />
      ) : (
        <NoAudioInput sendIcon={sendIcon} />
      )}
    </div>
  );
};

export default FullScreen;
