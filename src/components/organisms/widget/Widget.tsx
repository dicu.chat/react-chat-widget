import React from 'react';
import { useState } from 'react';
import Header from '../header/Header';
import Input from '../input/Input';
import NoAudioInput from '../noAudioInput/NoAudioInput';
import Messages from '../messages/Messages';
import IconButton from '../../atoms/iconButton/IconButton';
import './Widget.css';

export interface WidgetProps {
  websocketURL: string;
  audioEnabled?: boolean;
  userIcon?: Icon;
  closeIcon?: Icon;
  micIcon?: Icon;
  sendIcon?: Icon;
  widgetIcon?: Icon;
}

const Widget: React.FC<WidgetProps> = ({
  websocketURL,
  audioEnabled,
  userIcon,
  closeIcon,
  micIcon,
  sendIcon,
  widgetIcon,
}: WidgetProps) => {
  let [open, setOpen] = useState(false);

  const closeWidget = () => {
    setOpen(false);
  };

  const openWidget = () => {
    setOpen(true);
  };

  const closed = () => {
    return (
      <div className='WidgetButton'>
        <IconButton Icon={widgetIcon} onClick={openWidget} />
      </div>
    );
  };

  const opened = () => {
    return (
      <div className='WidgetContent'>
        <div className='WidgetHeader'>
          <Header
            userIcon={userIcon}
            closeCallback={closeWidget}
            closeIcon={closeIcon}
          />
        </div>
        <div className='WidgetMessagesAndInput'>
          <Messages websocketURL={websocketURL} />
          {audioEnabled ? (
            <Input sendIcon={sendIcon} micIcon={micIcon} />
          ) : (
            <NoAudioInput sendIcon={sendIcon} />
          )}
        </div>
      </div>
    );
  };
  return <div className='Widget'>{open ? opened() : closed()}</div>;
};

export default Widget;
