import React from 'react';
import { useState } from 'react';
import { sendTextMessage } from '../../../services/socket';
import InputButton from '../../atoms/inputButton/InputButton';
import Commands from '../../molecules/commands/Commands';
import './NoAudioInput.css';

interface NoAudioInputProps {
  sendIcon?: Icon;
}

const NoAudioInput: React.FC<NoAudioInputProps> = ({
  sendIcon,
}: NoAudioInputProps) => {
  const [text, setText] = useState('');

  const handleTextChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(event.target.value);
  };

  const handleSend = async () => {
    if (text) {
      sendTextMessage(text);
      setText('');
    }
  };

  const onEnterPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleSend();
    }
  };

  const handleButtonClick = () => {
    handleSend();
  };

  const handleCommandClose = () => {
    setText('');
  };

  const commands = () => {
    if (text.startsWith('/')) {
      return <Commands closeCallback={handleCommandClose} />;
    }
  };

  return (
    <div className='Input'>
      {commands()}
      <textarea
        className='InputTextarea'
        value={text}
        onChange={handleTextChange}
        onKeyDown={onEnterPress}
      />
      <InputButton showSend onClick={handleButtonClick} sendIcon={sendIcon} />
    </div>
  );
};

export default NoAudioInput;
