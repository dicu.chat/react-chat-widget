import React from 'react';
import FullScreen from './components/organisms/fullScreen/FullScreen';
import CloseIcon from './static/close.svg';
import SendIcon from './static/send.svg';

const WEBSOCKET_URL = 'wss://0f411020c4.a91ad78fad90f2d5aa4f51e0.com';

export default {
  title: 'Fullscreen',
  component: FullScreen,
};

export const Default = (): React.ReactNode => (
  <FullScreen websocketURL={WEBSOCKET_URL} audioEnabled />
);

export const SVGSwitch = (): React.ReactNode => (
  <FullScreen
    websocketURL={WEBSOCKET_URL}
    audioEnabled
    micIcon={CloseIcon}
    userIcon={SendIcon}
    sendIcon={CloseIcon}
  />
);

export const IMGLinkWithAudio = (): React.ReactNode => (
  <FullScreen
    websocketURL={WEBSOCKET_URL}
    audioEnabled
    userIcon={'https://picsum.photos/40'}
    micIcon={'https://picsum.photos/41'}
    sendIcon={'https://picsum.photos/40'}
  />
);

export const IMGLinkWithoutAudio = (): React.ReactNode => (
  <FullScreen
    websocketURL={WEBSOCKET_URL}
    userIcon={'https://picsum.photos/40'}
    sendIcon={'https://picsum.photos/40'}
  />
);
