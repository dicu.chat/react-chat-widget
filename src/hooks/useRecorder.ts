import { useState } from 'react';

let recorder: MediaRecorder | null;

const startRecorder = async () => {
  if (!recorder) {
    recorder = await requestRecorder();
  }
  recorder.start();
};

const stopRecorder = async () => {
  if (!recorder) {
    return null;
  }
  // Create promise for ondataavailable event
  const onDataAvailable = new Promise<BlobEvent>((resolve) => {
    // Typescript bug: Object possibly null
    if (!recorder) {
      return null;
    }
    return (recorder.ondataavailable = (event) => resolve(event));
  });

  recorder.stop();

  const event = await onDataAvailable;
  return event.data;
};

export const useRecorder = (): [
  boolean,
  number,
  () => Promise<void>,
  () => Promise<Blob | null>
] => {
  const [isRecoding, setIsRecoding] = useState(false);
  const [counter, setCounter] = useState<NodeJS.Timeout | null>(null);
  const [time, setTime] = useState(0);

  const startRecoding = async () => {
    await startRecorder();
    setIsRecoding(true);
    setCounter(
      setInterval(() => {
        setTime((time) => time + 1);
      }, 1000)
    );
  };

  const stopRecoding = async () => {
    if (counter) {
      clearInterval(counter);
    }

    const recording = await stopRecorder();

    setIsRecoding(false);
    setTime(0);

    if (!recording) {
      return null;
    }
    return recording;
  };

  return [isRecoding, time, startRecoding, stopRecoding];
};

const requestRecorder = async () => {
  const stream = await navigator.mediaDevices.getUserMedia({
    audio: true,
  });
  return new MediaRecorder(stream);
};
