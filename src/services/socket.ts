import openSocket, { Socket } from 'socket.io-client';
import { setMessageCallback, pushMessage } from './socketState';
import { FileTypes, ContentTypes, InternalMessage } from './types';
import {
  onConnect,
  onConnectError,
  onConnectFailed,
  onNewMessage,
  onUpdateSession,
  onUpdateCommands,
  onClose,
} from './socketIncoming';
import { voiceBlobToBase64 } from './audioTranspiler';

let socket: Socket;

export const startSocket = (websocketURL: string, onMessage: () => void) => {
  setMessageCallback(onMessage);
  if (!socket) {
    console.log('Opening websocket:', websocketURL);
    socket = openSocket(websocketURL);

    socket.on('connect', onConnect);

    socket.on('connect_error', onConnectError);

    socket.on('connect_failed', onConnectFailed);

    socket.on('new_message', onNewMessage);

    socket.on('update_session', onUpdateSession);

    socket.on('update_commands', onUpdateCommands);

    socket.on('close', onClose);
  }
};

export const sendStart = (session: any) => {
  socket.emit('start', { session: session });
};

export const sendTextMessage = (text: string) => {
  pushMessage({ message: { text }, files: [], buttons: [], isBot: false });
  socket.emit('new_message', {
    session: localStorage.getItem('session'),
    text,
  });
};

export const sendAudioMessage = async (blob: Blob, text?: string) => {
  // Store locally as url
  let message: InternalMessage = {
    message: {
      voice: {
        content: URL.createObjectURL(blob),
        mediaType: FileTypes.Voice,
        contentType: ContentTypes.Url,
      },
    },
    files: [],
    buttons: [],
    isBot: false,
  };
  if (text) {
    message.message.text = text;
  }
  pushMessage(message);

  // Send to server
  // e.g. ['audio/webm', 'opus']
  const type = blob.type.split(';codecs=');

  socket.emit('new_voice', {
    session: localStorage.getItem('session'),
    voice: await voiceBlobToBase64(blob),
    extension: type[0],
    codec: type[1],
  });
};
