import { InternalMessage, Commands, Command } from './types';

let messages: InternalMessage[] = [];
let messageCallback: () => void;

let commands: Command[] = [];

export const setMessageCallback = (onMessage: () => void) => {
  messageCallback = onMessage;
};

export const setCommands = (socketCommands: Commands) => {
  for (const name in socketCommands) {
    commands = [...commands, { name, description: socketCommands[name] }];
  }
  console.log('[SocketState] Updated commands', commands);
};

export const pushMessage = (message: InternalMessage) => {
  messages = [...messages, message];
  messageCallback();
};

export const getMessages = () => {
  return messages;
};

export const getCommands = () => {
  return commands;
};
