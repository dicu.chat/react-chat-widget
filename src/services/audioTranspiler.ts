import { encode, decode } from 'base64-arraybuffer';

export const voiceBlobToBase64 = async (blob: Blob) => {
  const buffer = await blob.arrayBuffer();
  const base64 = encode(buffer);
  return base64;
};

export const base64ToURL = async (
  base64: string,
  container = 'audio/webm',
  codec = 'opus'
) => {
  const buffer = decode(base64);
  const blob = new Blob([buffer], {
    type: `${container};codecs="${codec}"`,
  });
  return URL.createObjectURL(blob);
};
