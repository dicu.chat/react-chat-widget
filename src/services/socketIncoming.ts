import { MessageResponseBody, Commands } from './types';
import { pushMessage, setCommands } from './socketState';
import { sendStart } from './socket';

export const onConnect = () => {
  let session = localStorage.getItem('session');
  console.log('Connected! Sending start event with session:', session);
  sendStart(session);
};

export const onConnectError = (err: any) => {
  console.log(err);
};

export const onConnectFailed = (err: any) => {
  console.log(err);
};

export const onNewMessage = (event: MessageResponseBody) => {
  console.log('Received new_message event:', event);
  pushMessage(event);
};

export const onUpdateSession = (event: any) => {
  console.log('Received update_session event:', event);
  localStorage.setItem('session', event.session);
};

export const onUpdateCommands = (event: Commands) => {
  console.log('Received update_commands event:', event);
  setCommands(event);
};

export const onClose = () => {
  console.log('The connection has been closed successfully.');
};
