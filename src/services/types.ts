export interface User {
  userId: string;
  firstName?: string;
  lastName?: string;
  username?: string;
  langCode?: string;
}

export interface Chat {
  chatId: string;
  name?: string;
  username?: string;
}

export enum FileTypes {
  Voice = 'voice',
  Image = 'image',
  Gif = 'gif',
  Video = 'video',
  Document = 'document',
  Location = 'location',
}

export enum ContentTypes {
  Bytes = 'bytes',
  Url = 'url',
}

export interface File {
  // Generic type of the file for frontends to handle better.
  mediaType: FileTypes;
  // Depending on 'content_type' different actions will be required to use operate the file.
  contentType: ContentTypes;
  // Content means different things depending on the 'type':
  //   - 'bytes': base64-encoded bytes contents of the voice file
  //   - 'url': 'http://' or 'https://' type url to the file
  content: string;
  // Additional attributes for files.
  extension?: string;
  codec?: string;
}

export interface Message {
  text?: string;
  // Note: voice is a file with 'mediaType' set to 'voice'.
  voice?: File;
}

enum ButtonTypes {
  Text = 'text',
  Url = 'url',
}

export interface Button {
  text: string;
  // This property aims to notify frontend that the button should be treated as a link, and
  // the content of the link is in the 'value' property.
  buttonType?: ButtonTypes;
  // Contains additional value for the button (e.g. link, in case of 'url' type).
  value?: string;
}

interface Dictionary<T> {
  [Key: string]: T;
}

export interface MessageResponseBody {
  user: User;
  chat: Chat;
  message: Message;
  isBot: boolean;
  // Items below are optional, but they represent, at least, an empty object (dict or list).
  files: File[];
  buttons: Button[];
  // You can write any additional information to this context when sending a request, so then server response will contain it.
  // Note: Context size is limited to 4KB, if it's more than that, the request will be rejected (TBD).
  context: Dictionary<string>;
}

export interface Commands {
  [key: string]: string;
}

export interface Command {
  name: string;
  description: string;
}

// Store
export interface InternalMessage {
  message: Message;
  files: File[];
  buttons: Button[];
  isBot: boolean;
}
